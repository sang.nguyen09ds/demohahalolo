<?php
/**************************************************
* Wrapper class to use CheapEmail API
* Created by Nguyen Ngo
**************************************************/
namespace App\Libraries\CheapEmail;

/**
 * Class ApiRequestor
 *
 * @package CheapEmail
 */
class ApiRequestor {

    private $_apiUrl;
    private $_apiToken;
    private $_apiKey;

    public function __construct($apiUrl, $apiToken, $apiKey) {
        $this->_apiUrl = $apiUrl;
        $this->_apiToken = $apiToken;
        $this->_apiKey = $apiKey;
    }

    /**
     * Request
     */
    public function request($url, $params = array(), $is_post = false) {
        $url = $this->_apiUrl . $url;
        list($body, $httpcode) = $this->requestData($url, $params, $is_post);
        return $this->_interpretResponse($body, $httpcode);
    }

    /**
     * Request Data
     */
    private function requestData($url, $params, $is_post) {
        $ch = curl_init();
        $headers  = array(
            'Api-Token:' . $this->_apiToken, 
            'Api-Key:' . $this->_apiKey
        );

        if ($is_post) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $url = !empty($params) ? $url . '?' . http_build_query($params) : $url;
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        $body = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return array($body, $httpcode);
    }

    /**
     * @throws Error\InvalidRequest if the error is caused by the user.
     * @throws Error\Authentication if the error is caused by a lack of
     *    permissions.
     * @throws Error\RateLimit if the error is caused by too many requests
     *    hitting the API.
     * @throws Error\Api otherwise.
     */
    public function handleApiError($body, $code, $resp) {
        $msg = @$resp->message != '' ? $resp->message : 'No message';

        switch ($code) {
            case 404:
                throw new Error\InvalidRequest($msg, $code);
            case 401:
                throw new Error\Authentication($msg, $code);
            case 429:
                throw new Error\RateLimit($msg, $code);
            default:
                throw new Error\Api($msg, $code);
        }
    }

    /**
     * Interpet Response
     */
    private function _interpretResponse($body, $code) {
        $resp = json_decode($body);
        $jsonError = json_last_error();
        if ($resp === null && $jsonError !== JSON_ERROR_NONE) {
            $msg = "Invalid response body from API: $body "
              . "(HTTP response code was $code, json_last_error() was $jsonError)";
            throw new Error\Api($msg, $code);
        }
        if (isset($resp->status) && $resp->status == 'error') {
            throw new Error\Api(isset($resp->message) ? $resp->message : 'Response Status: error!', $code);
        }

        if ($code < 200 || $code >= 300) {
            $this->handleApiError($body, $code, $resp);
        }
        return $resp;
    }
}
