<?php

namespace App\Libraries\CheapEmail\Error;

class InvalidRequest extends Base
{
    public function __construct($message) {
        parent::__construct($message);
    }
}
