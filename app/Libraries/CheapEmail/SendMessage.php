<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 11/1/2019
 * Time: 2:01 PM
 */

namespace App\Libraries\CheapEmail;


class SendMessage
{
    protected $base_url = 'https://app.cheapemail.co/api/v2/';
    protected $api_token = 'eyJpdiI6ImFoc1crOVdpM29OUkhvYWpcLzF4SWtBPT0iLCJ2YWx1ZSI6IkJkYzRnUE1OMWZzUmdhbWtnMDBCUEtYcXNVSUd6UVVNOVhadEtocjVFcDBPNDJtWmoxbzkzV2w3K2t2a0h0WU4iLCJtYWMiOiI5OGI4NWQ5NzdjOWYyZGUzZmE3YzdhNTQ5YTkyMjQ1YmMwY2YzMTIwYzFhOWRiZDc5MDc0OTE4ZDQzNTE5MjViIn0=';
    protected $api_key = '7162451cc784459f04b98987d615f362';

    public function __construct($mode = 'LIVE_MODE')
    {
        if ($mode == 'TEST_MODE') {
            $this->base_url = 'https://huong-newsletter.gitview.net/api/v2/';
            $this->api_token = 'eyJpdiI6ImFoc1crOVdpM29OUkhvYWpcLzF4SWtBPT0iLCJ2YWx1ZSI6IkJkYzRnUE1OMWZzUmdhbWtnMDBCUEtYcXNVSUd6UVVNOVhadEtocjVFcDBPNDJtWmoxbzkzV2w3K2t2a0h0WU4iLCJtYWMiOiI5OGI4NWQ5NzdjOWYyZGUzZmE3YzdhNTQ5YTkyMjQ1YmMwY2YzMTIwYzFhOWRiZDc5MDc0OTE4ZDQzNTE5MjViIn0=';
            $this->api_key = '7162451cc784459f04b98987d615f362';
        }
    }

    /**
     * Sync Emails
     */
    public function syncSMS($data)
    {
        try {
            $requestor = new ApiRequestor($this->base_url, $this->api_token, $this->api_key);
            return $requestor->request('newsletter/email/add', array('data' => gzencode(base64_encode(json_encode($data)))), true);
        } catch (Error\InvalidRequest $e) {
            // if the error is caused by the user.
            return NULL;
        } catch (Error\Authentication $e) {
            // if the error is caused by a lack of permissions
            return NULL;
        } catch (Error\RateLimit $e) {
            // if the error is caused by too many requests
            return NULL;
        } catch (Error\Api $e) {
            // otherwise.
            return NULL;
        }
    }

    /**
     * @param $data
     * @return mixed|null
     */
    public function sendSMS($data)
    {
        try {
            $requestor = new ApiRequestor($this->base_url, $this->api_token, $this->api_key);
            return $requestor->request('operational-sms/send-message', array('data' => gzencode(base64_encode(json_encode($data)))), true);
        } catch (Error\InvalidRequest $e) {
            // if the error is caused by the user.
            return NULL;
        } catch (Error\Authentication $e) {
            // if the error is caused by a lack of permissions
            return NULL;
        } catch (Error\RateLimit $e) {
            // if the error is caused by too many requests
            return NULL;
        } catch (Error\Api $e) {
            // otherwise.
            return NULL;
        }
    }
}
