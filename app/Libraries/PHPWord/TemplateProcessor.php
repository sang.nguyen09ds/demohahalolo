<?php
namespace App\Libraries\PHPWord;

use PhpOffice\PhpWord\TemplateProcessor as BaseTemplateProcessor;

class TemplateProcessor extends BaseTemplateProcessor
{


    /**
     * Clone a block.
     *
     * @param string $blockname
     * @param int $clones
     * @param bool $replace
     *
     * @return string|null
     */
    public function cloneBlock($blockname, $clones = 1, $replace = true)
    {
        $xmlBlock = null;
        preg_match(
            '/(<\?xml.*)(\${' . $blockname . '})(.*)(\${\/' . $blockname . '})/is',
            $this->tempDocumentMainPart,
            $matches
        );

        if (isset($matches[3])) {
            $xmlBlock = $matches[3];
            $cloned = array();
            for ($i = 1; $i <= $clones; $i++) {
                $cloned[] = $xmlBlock;
            }

            if ($replace) {
                $this->tempDocumentMainPart = str_replace(
                    $matches[2] . $matches[3] . $matches[4],
                    implode('', $cloned),
                    $this->tempDocumentMainPart
                );
            }
        }

        return $xmlBlock;
    }

}