<?php


namespace App\Models;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserLog extends Model
{
    protected $table = 'users_log';
    public $timestamps = false;

    public static function saveLogUser($input)
    {
        $param = [
            'access_token' => !empty($input['access_token']) ? $input['access_token'] : null,
            'token_type' => !empty($input['token_type']) ? $input['token_type'] : null,
            'refresh_token' => !empty($input['refresh_token']) ? $input['refresh_token'] : null,
            'expires_in' => !empty($input['expires_in']) ? $input['expires_in'] : null,
            'scope' => !empty($input['scope']) ? $input['scope'] : null,
            'nv103' => !empty($input['nv103']) ? $input['nv103'] : null,
            'nv104' => !empty($input['nv104']) ? $input['nv104'] : null,
            'nv105' => !empty($input['nv105']) ? $input['nv105'] : null,
            'nv107' => !empty($input['nv107']) ? $input['nv107'] : null,
            'nv108' => !empty($input['nv108']) ? $input['nv108'] : null,
            'nv111' => !empty($input['nv111']) ? $input['nv111'] : null,
            'nv120' => !empty($input['nv120']) ? $input['nv120'] : null,
            'nn130' => !empty($input['nn130']) ? $input['nn130'] : null,
            'nn140' => !empty($input['nn140']) ? $input['nn140'] : null,
            'lang' => !empty($input['lang']) ? $input['lang'] : null,
            'currency' => !empty($input['currency']) ? $input['currency'] : null,
            'is_active' => 1,
            'created_at' => date("Y-m-d H:i:s", time()),
            'created_by' => 'User # '. Auth::guard('admin')->user()->id,
        ];
        return UserLog::insert($param);
    }
}
