<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function testAlways()
    {
        $user = User::where('id', 4)->first();
    }

    public  function cart()
    {
        return view('admin.cards');
    }
    public  function buttons()
    {
        return view('admin.buttons');
    }
}
