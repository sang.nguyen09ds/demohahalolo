<?php


namespace App\Http\Controllers;


use App\Libraries\CheapEmail\CheapEmail;
use App\Libraries\Login\ApiRequestor;
use App\Libraries\Login\LoginApi;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\False_;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $base_url = IP_CLIENT . '/api/oauth/token';
    protected $grant_type = 'authorization_code';
    protected $access_token = 'tJ8x6mg-x9S1-MACfZ-1juxvbJU'; // hard code
    protected $response_type = 'code'; // hard code
    protected $client_id = 'https://3rd-auth-demo.hahalolo.com';
    protected $url_logout = IP_CLIENT . '/api/logout/oneDevice';
    protected $clientId_Secret = '5ef56c1d353f0026019c4da9:passHaHa';

    public function __construct()
    {
        $this->clientId_Secret = base64_encode($this->clientId_Secret);
    }

    public function API()
    {
        //clientId:ClientSecret
        //NWVmNTZjMWQzNTNmMDAyNjAxOWM0ZGE5OnBhc3NIYUhh
        return Http::withHeaders([
            'Authorization' => 'Basic ' . 'N2NlMmM3NGQtNjBlZC00Nzk3LThkNTQtYzU5ZmQyZTJiZTE5',
            'uci' => 'sWRD2Im5q6FhMjOWRSeOYEkTZWZxPDHOP6xUwS+XFKY=',
            'Content-Type' => 'application/json'
        ]);
    }

    public function APIAuth()
    {
        return Http::withHeaders([
            //'Content-Type' => 'application/json'
        ]);
    }

    public function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('login.login');
    }

    /**
     * @param Request $request
     * @return \Exception|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(Request $request)
    {
        $input = $request->all();
        if (!empty($input)) {
            try {
                $response = $this->API()->post($this->base_url, [
                    'grant_type' => !empty($input['grant_type']) ? $input['grant_type'] : null,
                    'username' => !empty($input['username']) ? $input['username'] : null,
                    'password' => !empty($input['password']) ? $input['password'] : null,
                ]);
                $checkStatus = $this->status($response->status());
                if ($checkStatus === 200) {
                    $data = $response->json();
                    $res = User::saveUser($data);
                    Auth::guard('admin')->loginUsingId($res);
                    return redirect('/admincp');
                } else {
                    return redirect()->back()->with('status', 'Email hoặc mật khẩu không đúng !');
                }
            } catch (\Exception $e) {
                return $e;
            }
        }
        return redirect()->route('getLogin');
    }

    /**
     * call api from the 3rd party to check if it has been authenticated, it will pass into the system
     * @param Request $request
     * @return \Exception|int|string
     */
    public function getLoginThird(Request $request)
    {
        die('xx 000000');
        $input = $request->all();
        if (!empty($input['code'])) {
            try {

                $response = $this->API()->post($this->base_url, [
                    'grant_type' => $this->grant_type,
                    'code' => $input['code'],
                    'redirect_uri' => IP_CURRENT,
                ]);
                //dd($input);
                $checkStatus = $this->status($response->status(), $input['code']);
                if ($checkStatus === 200) {
                    $data = $response->json();
                    $res = User::saveUser($data);
                    Auth::guard('admin')->loginUsingId($res);
                    //return redirect('/admincp');
                    return "<script>
                   window.opener.location = '/admincp';
                   window.close();
                  </script>";
                } else {
                    return $this->status($response->status(), $input['code']);
                }
            } catch (\Exception $e) {

                return $e;
            }
        } else {
            return redirect()->route('getLogin');
            // return 'Deny Login failure';
        }
    }


    public function getLoginHaHaLoLo()
    {
        return view('login.loginHL');
    }

    public function postLoginHaHaLoLo(Request $request)
    {
        $input = $request->all();
        die('zz');
        $response = Http::post(IP_CLIENT . '/api/login', [
            'username' => !empty($input['username']) ? $input['username'] : null,
            'password' => !empty($input['password']) ? $input['password'] : null,
        ]);
        // http://10.0.2.128/api/login
    }

    /**
     *Check the status of the returned API.
     * @param $status
     * @param $code
     * @return int|string
     */
    public function status($status, $code = null)
    {
        $res = '';
        switch ($status) {
            case 200:
            {
                return 200;
                // break;
            }
            case 400:
                return 'error 400 Invalid authorization code:' . $code;
            //break;
            case 401:
                //return response()->view('page/404');
                return 'UserDetailsService returned null, which is an interface contract violation';
                break;
            case 404:
            {
                //  return response()->view('page/404');
                return 'Internal Server Error 404';
            }
            case 500:
            {
                //return response()->view('page/500');
                return '"Internal Server Error 500';
            }
        }
        return $res;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        $access_token = !empty(Auth::guard('admin')->user()) ? Auth::guard('admin')->user()->access_token : null;
        $response = Http::get($this->url_logout, [
            'access_token' => $access_token,
        ]);
        //dd($response);
        // Auth::guard()->logout();
        // return redirect('/');
        //   dd($response);
//        $response = $this->API()->get($this->url_logout, [
//            'access_token' => $access_token,
//        ]);
        // Auth::guard()->logout();
        //return redirect('/');
        //  dd($response);
        if ($response->status() == 200) {
            Auth::guard()->logout();
            return redirect('/');
        }
    }
    //public  function  test
}
