<?php


namespace App\Http\Middleware;


use App\Http\Controllers\LoginController;
use App\Models\UserLog;
use App\User;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use function Aws\dir_iterator;

class checkLogin
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected $base_url_check = IP_CLIENT . '/aut/oauth/authorize';
    protected $get_token_url = IP_CLIENT . '/aut/oauth/token';
    protected $grant_type = 'refresh_token';

    public function APIAuth()
    {
        return Http::withHeaders([
            'Authorization' => 'Basic ' . 'aGFoYWxvbG8tY2xpZW50LWlkOnBhc3N3b3Jk',
            'uci' => 'sWRD2Im5q6FhMjOWRSeOYEkTZWZxPDHOP6xUwS+XFKY=',
            'Content-Type' => 'application/json',
        ]);
    }

    public function handle($request, \Closure $next)
    {

        $getCode = Http::get($this->base_url_check, [
            'access_token' => 'y8249ZXDQrg5Nh79qhfPkx42-gg',
            'grant_type' => 'authorization_code',
            'response_type' => 'code',
            'client_id' => 'hahalolo-client-id',
        ]);
        $codeUrl = $getCode->transferStats->getHandlerStats()['url'];
        $newParams = parse_url($codeUrl, PHP_URL_QUERY);
        $newParams = explode("code=", $newParams);

        if (!empty($newParams[1])) {

            $token = $this->APIAuth()->post($this->get_token_url.'?grant_type=authorization_code&code='.$newParams[1].'&redirect_uri=https://test-api-cloud-gateway.hahalolo.com/fallback', [
                'grant_type' => 'authorization_code',
                'code' => $newParams[1],
                'redirect_uri' => 'https://test-api-cloud-gateway.hahalolo.com/fallback',
            ]);

            view()->share('currentUser', ['user'=>$token->body()]);
            return $next($request);
        }
        else {
            return redirect('/');
        }
//        if (Auth::guard('admin')->check()) {
//            $refresh_token = !empty(Auth::guard('admin')->user()->refresh_token) ? Auth::guard('admin')->user()->refresh_token : null;
//            $token = !empty(Auth::guard('admin')->user()->refresh_token) ? Auth::guard('admin')->user()->access_token : null;
//            view()->share('currentUser', Auth::guard('admin')->user());
//
//            //This will check the token's expiry date, if the token expires, it will call the token renewal API for the user.
//            $resCheckToken = Http::get($this->base_url_check, [
//                'token' => $token
//            ]);
//            if ($resCheckToken->status() !== 200) {
//                $res = $this->APIAuth()->post($this->base_url, [
//                    'grant_type' => $this->grant_type,
//                    'refresh_token' => $refresh_token,
//                ]);
//                if ($res->status() == 200) {
//                    $data = $res->json();
//                    $this->saveUser($data);
//                    return $next($request);
//                } else {
//                    return LoginController::status($res->status());
//                }
//            } else {
//                return $next($request);
//            }
//        } else {
//            return redirect('/');
//        }
    }

    public function saveUser($data)
    {
        try {
            DB::beginTransaction();
            User::saveUser($data);
            UserLog::saveLogUser($data);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return $ex->getMessage();
        }
        return $data;
    }
}
