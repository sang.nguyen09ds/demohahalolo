<?php

namespace App;

use App\Models\Permission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access_token',
        'token_type',
        'refresh_token',
        'expires_in',
        'scope',
        'nv103',
        'nv104',
        'nv105',
        'nv107',
        'nv108',
        'nv111',
        'nv120',
        'nn130',
        'nn140',
        'lang',
        'currency',
        'is_active',
        'deleted',
        'created_at',
        'created_by',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $input
     * @return mixed
     */
    public static function saveUser($input)
    {
        $user = !empty($input) ? User::where('nv108', $input['nv108'])->first() : null;
        if (!$user) {
            $param = [
                'access_token' => !empty($input['access_token']) ? $input['access_token'] : null,
                'token_type' => !empty($input['token_type']) ? $input['token_type'] : null,
                'refresh_token' => !empty($input['refresh_token']) ? $input['refresh_token'] : null,
                'expires_in' => !empty($input['expires_in']) ? $input['expires_in'] : null,
                'scope' => !empty($input['scope']) ? $input['scope'] : null,
                'nv103' => !empty($input['nv103']) ? $input['nv103'] : null,
                'nv104' => !empty($input['nv104']) ? $input['nv104'] : null,
                'nv105' => !empty($input['nv105']) ? $input['nv105'] : null,
                'nv107' => !empty($input['nv107']) ? $input['nv107'] : null,
                'nv108' => !empty($input['nv108']) ? $input['nv108'] : null,
                'nv111' => !empty($input['nv111']) ? $input['nv111'] : null,
                'nv120' => !empty($input['nv120']) ? $input['nv120'] : null,
                'nn130' => !empty($input['nn130']) ? $input['nn130'] : null,
                'nn140' => !empty($input['nn140']) ? $input['nn140'] : null,
                'lang' => !empty($input['lang']) ? $input['lang'] : null,
                'currency' => !empty($input['currency']) ? $input['currency'] : null,
                'is_active' => 1,
                'created_at' => date("Y-m-d H:i:s", time()),
            ];
            $userId = User::insertGetId($param);
        } else {
            $user->access_token = !empty($input['access_token']) ? $input['access_token'] : $user->access_token;
            $user->refresh_token = !empty($input['refresh_token']) ? $input['refresh_token'] : $user->refresh_token;
            $user->updated_at = date("Y-m-d H:i:s", time());
            $user->save();
        }
        return !empty($user) ? $user->id : $userId;
    }

}
