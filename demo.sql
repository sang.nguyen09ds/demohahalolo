/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.13-MariaDB : Database - demo_hahalolo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`demo_hahalolo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `demo_hahalolo`;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `access_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires_in` bigint(20) NOT NULL DEFAULT 0,
  `scope` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv103` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv104` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv105` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv107` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv108` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv111` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nv120` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nn130` int(11) DEFAULT NULL,
  `nn140` int(11) DEFAULT NULL,
  `lang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) unsigned DEFAULT 0,
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `access_token_nv108` (`access_token`,`nv108`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`access_token`,`token_type`,`refresh_token`,`expires_in`,`scope`,`nv103`,`nv104`,`nv105`,`nv107`,`nv108`,`nv111`,`nv120`,`nn130`,`nn140`,`lang`,`currency`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`) values 
(4,'90465503-49b4-4dcf-a7ca-c5707c73e26b','bearer','ac556afa-15b4-44ec-903c-0b61064952d6',99,'read','アリューシア','アセイラム','アリューシア ヴァース アセイラム','9642B32CDAB01C8664B70D7C9E27C870','EC3A4FCC28E7FD29FCC7FCC0A20F27483961FB16D527F9F71A848C6A763FFC79','VN','https://test-media.hahalolo.com/5d91810bbc5026044559da1b/KAI3kvWrUerCeA33.png',1,NULL,'vi','VND',1,0,'2020-06-24 08:05:23',NULL,'2020-07-27 07:50:48');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
