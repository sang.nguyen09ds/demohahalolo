<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::prefix('/')->group(function () {
    Auth::routes();
    Route::get('/', ['as' => 'getLogin', 'uses' => 'LoginController@getLogin']);
    Route::get('/login', ['as' => 'getLoginThird', 'uses' => 'LoginController@getLoginThird']);
//    Route::get('/login-admin', ['as' => 'getLogin', 'uses' => 'LoginController@postLogin']);
    Route::post('/login-admin', ['as' => 'postLogin', 'uses' => 'LoginController@postLogin']);
//    Route::get('/login-hahalolo-1', ['as' => 'getLoginHahalolo', 'uses' => 'LoginController@getLoginHaHaLoLo']);
//    Route::post('/login-hahalolo', ['as' => 'postLoginHahalolo', 'uses' => 'LoginController@postLoginHaHaLoLo']);

    Route::middleware(['checkLogin'])->group(function () {
        Route::get('/login-hahalolo', ['as' => 'page_admin', 'uses' => 'HomeController@index']);
        Route::get('/logout', ['as' => 'getLogout', 'uses' => 'LoginController@getLogout']);
        Route::get('/carts', ['as' => 'carts', 'uses' => 'HomeController@cart']);
        Route::get('/buttons', ['as' => 'buttons', 'uses' => 'HomeController@buttons']);
    });
});
Route::get('/testAlways', ['as' => 'getTestAlways', 'uses' => 'HomeController@testAlways']);
