<!DOCTYPE html>
<html>
<head>
    <title>Be right back.</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-md-8">
        <div class="text-error">
            <h1 class="text-primary">Oh no !</h1>
            <h1 class="text-gray">Error <span class="text-danger">404</span> Page not found</h1>
            <p>Permission Denied!</p>
            <a href="#"><i class="icon-arrow-right-circle"></i>Return Home Page</a>
        </div>
    </div>
</div>
</body>
</html>
