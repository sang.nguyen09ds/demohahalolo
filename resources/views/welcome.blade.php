<!doctype html>
<html lang="en">
<head>
    <!-- Additional for each module -->
    <title>Hahalolo - DEVELOPERS DEVELOPERS</title>
    <meta name="description" content="">
    <meta property="og:image" content="">
    <!-- header base -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="https://test-html.hahalolo.com/assets/img/logo/favicon/favicon-32.ico"
          type="image/x-icon"/>

    <!-- load base css: gồm các BS, FAS -->

    <!-- custom meta tag for Web app mobile -->
    <meta name="theme-color" content="#166986">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <link rel="apple-touch-startup-image"
          href="https://test-html.hahalolo.com/assets/img/logo/favicon/favicon-128.png"/>
    <link rel="apple-touch-icon" href="https://test-html.hahalolo.com/assets/img/logo/favicon/favicon-128.png"/>

    <!-- <link rel="manifest" href="header/common/manifest.json"> -->
    <script type="text/javascript">
        const base_url = 'https://test-html.hahalolo.com/';
    </script>

    <script type="text/javascript" src="https://test-html.hahalolo.com/assets/js/custom/theme/theme.js"></script>
    <!-- Additional for each module -->
    <title>Hahalolo for Developers</title>
    <meta name="description" content="">
    <meta property="og:image" content="">
    <link rel="stylesheet" href="assets/css/developers.css">
</head>
<body class="dev">
<header class="dev-menu fixed-top opacity" include-html="header/developers/home-menu-top.html"></header>
<main class="dev-container" id="load-page-container">
    <!-- Load page content -->
</main>


<link rel="stylesheet" type="text/css" href="https://test-html.hahalolo.com/assets/js/3rd/selectize/selectize.css"/>
<link rel="stylesheet" type="text/css"
      href="https://test-html.hahalolo.com/assets/js/3rd/air-datepicker/css/datepicker.min.css"/>
<script src="https://test-html.hahalolo.com/assets/js/3rd/jquery.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/popper.min.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/bootstrap.bundle.min.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/selectize/selectize.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/air-datepicker/js/datepicker.min.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/air-datepicker/js/i18n/datepicker.en.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/air-datepicker/js/i18n/datepicker.vi.js"></script>
<script src="https://test-html.hahalolo.com/assets/fonts/flags/js/languages.js"></script>
<script src="https://test-html.hahalolo.com/assets/fonts/flags/js/currencies.js"></script>
<script src="https://test-html.hahalolo.com/assets/fonts/flags/js/countries.js"></script>
<script src="https://test-html.hahalolo.com/assets/fonts/flags/js/phone-code.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/3rd/currency/currency.min.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/common.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/dev.js"></script>
<script src="https://test-html.hahalolo.com/assets/js/custom/validation/validation.js"></script>
<script src="https://test-html.hahalolo.com/parts/login/login.js"></script>
<script src="assets/js/custom/scroll-sidebar/scroll-sidebar.js"></script>
<!-- <script src="assets/js/custom/scroll-sidebar/scroll-sidebar-dist.min.js"></script> -->
<script src="assets/js/custom/menu/mmenu.js"></script>

<!-- js chat tab -->
<script defer src="parts/messenger/library/js/chattab.js"></script>


<!-- menu lối tắt dẫn đến các page management (chỉ dev) -->
<div class="bounce">
    <ul class="list-group shadow">
        <a class="list-group-item" href="business.php?page=dashboard&fname=business-account" target="_blank">Business
            dashboard</a>
        <a class="list-group-item" href="tour.php?page=settings&fname=general" title="Go to Tour management"
           target="_blank">Tour</a>
        <a class="list-group-item" href="hotel.php?page=settings&fname=general" title="Go to Hotel management"
           target="_blank">Hotel</a>
        <a class="list-group-item" href="shop.php?page=settings&fname=general" title="Go to Shop management"
           target="_blank">Shop</a>
        <a class="list-group-item" href="backend/dashboards.php?page=halosearch" target="_blank">Backend</a>
        <a class="list-group-item" href="developers.php?page=developers" target="_blank">Developers</a>
        <a class="list-group-item" href="mail.php" target="_blank">Mail templates</a>
        <div class="list-group-item" onclick="openModalVerifyAccount();">Modal Verify account</div>
        <div class="list-group-item" onclick="openModalVerifyAccountNew();">Modal xác thực tài khoản mới</div>
        <div class="list-group-item" onclick="loginTip();">Login tip modal</div>
    </ul>
    <div class="bounce-btn" title="Shortcut link to projects">
        <i class="fas fa-plus"></i>
    </div>
</div>

<style>
    .bounce {
        position: fixed;
        left: 1rem;
        bottom: 4rem;
        z-index: 1000;
        cursor: pointer;
    }

    .bounce:hover .list-group {
        display: flex;
    }

    .bounce-btn {
        width: 40px;
        height: 40px;
        background: var(--danger);
        color: var(--white);
        border-radius: 50%;
        box-shadow: var(--shadow-sm);
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 1rem;
    }

    .bounce .list-group {
        display: none;
        margin-bottom: .5rem;
    }

    .bounce .list-group-item {
        font-size: 85%;
        font-weight: 500;
    }

    .bounce .list-group-item:hover {
        background-color: var(--light-bg);
    }
</style>

<script src="parts/modal/newsfeed/modal__mediascreen.js"></script>
<script src="parts/dropdown/dropdown-notif.js"></script>
<script src="assets/js/custom/page/developers.js"></script>
</body>
</html>
