<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>WELCOME TO HAHALOLO</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link href="https://getbootstrap.com/docs/4.0/examples/signin/signin.css" rel="stylesheet" crossorigin="anonymous"/>
</head>

<body>
<div class="container">
    <div style="width:600px;margin-left: auto;margin-right: auto;margin-top:24px;padding: 24px;">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-user"></i> WELCOME TO HAHALOLO
            </div>
            <div class="card-block" style="padding: 24px;">
                <form action="{{'http://10.0.1.83:8887/oauth2/api/login'}}" name="f"  role="form" method="POST">
                    <!-- form horizontal acts as a row -->
                    <fieldset>
                        <!-- Thymeleaf + Spring Security error display -->
                        <!--     <div th:if="${param.error}" class="alert alert-danger">
                                Invalid username and password.
                            </div>

                            <div th:if="${param.logout}" class="alert alert-success">
                                You have been logged out.
                            </div> -->

                        <!-- Login Controls -->
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username"
                                   placeholder="Username">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Password">
                        </div>

                        <!--   <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="checkRememberMe" name="checkRememberMe">
                              <label class="form-check-label" for="checkRememberMe">Remember me?</label>
                          </div> -->

                        <!-- Login Button -->
                        <div class="form-actions" style="margin-top: 12px;">
                            <button type="submit" class="btn btn-success">Log in</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
