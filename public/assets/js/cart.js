var domain = "http://e-shop.local/";
var route = domain + '/home/products/detail';

loadCartItem();
function addToCart(url) {

    event.preventDefault();
    jQuery.ajax({
        url: url,
        type: "GET",
        success:function(data){
            loadCartItem();
            alert(data.status);

        },
        error:function (){
            alert("Có lỗi , không thể thêm vào giỏ hàng");
        }
    });

}

function  updateQuantityCart ($event) {
    $event.preventDefault();
    var form = $event.target;
    var formData =  $(form).serialize();

    console.log('formData ', formData);
    jQuery.ajax({
        url: domain + '/home/cart/update/quantity',
        type: "POST",
        data : formData,
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        success:function(data){
            loadCartItem();
            alert(data.status);
        },
        error:function (){
            alert("Có lỗi , không thể sửa số lượng sản phẩm");
        }
    });

}
function deleteItemCart(id) {
    event.preventDefault();
    jQuery.ajax({
        url: domain + "/home/cart/delete/" + id,
        type: "GET",
        success:function(data){
            var  cartTotal = Object.keys(data).length;
            $('.cart-total-number').addClass("has-item").html(cartTotal);
            alert(data.status);
            loadCartItem();
            appendListCartPage(data,cartTotal);
            appendCheckout(data,cartTotal);
        },
        error:function (){
            alert("Có lỗi , không thể xóa sản phẩm trong giỏ hàng");
        }
    });

}

function loadCartItem() {

    jQuery.ajax({
        url: domain + '/home/cart-list',
        type: "GET",
        success:function(data){

        var  cartTotal = Object.keys(data).length;
            $('.cart-total-number').addClass("has-item").html(cartTotal);
            appendListCart(data,cartTotal);
            appendListCartPage(data,cartTotal);
            appendCheckout(data,cartTotal);
            console.log('cart item ', data , cartTotal);
        },
        error:function (){
            alert("Có lỗi , không thể xóa sản phẩm trong giỏ hàng");
        }
    });
}


// current-cart-list
function appendListCart(cartData,cartTotal) {
    $cartList = $("#current-cart-list");
    $cartTotal = $("#current-cart-list-total");
    var price = 0;
    var total = 0;
    $.each(cartData, function (i, item) {
        price = (item['price'] * item['quantity']);
        total += price;
    });

    $cartList.html('');

        $.each(cartData, function (i, item, ) {
        $cartList.append(`
                
                    <li class="single-cart-item media">
                    <div class="shopping-cart-img mr-4">
                        <a href=" `+ route +`/`+ item['id']  +`">
                            <img class="img-fluid" alt="Cart Item" src="`+ domain + '/uploads/' + item['thumbnail'] +`">
                            <span class="product-quantity">`+ item['quantity'] +`</span>
                        </a>
                    </div>
                    <div class="shopping-cart-title media-body">
                        <h4><a href=" `+ route +`/`+ item['id']  +`">`+ item['title'] +`</a></h4>
                        <p class="cart-price">`+ number_format(item['price']) +`  VND</p>
                        <div class="product-attr">
                            <span>Size: S</span>
                            <span>Color: Black</span>
                        </div>
                    </div>
                    <div class="shopping-cart-delete">
                        <a onclick="deleteItemCart(`+ item['id'] +`)" href="javascript:void(0);"><i class="ion ion-md-close"></i></a>
                    </div>
                    </li>
               
        `);
    });
    $cartTotal.html('');
     $cartTotal.append(`
                        <h4>Sub-Total : <span> `+ number_format(total) +` VND</span></h4>
                        <h4>Total : <span> `+  number_format(total)  +` VND</span></h4>
    `);
}
function appendListCartPage(cartData,cartTotal) {
    $cartList = $("#cart-list-page");
    $cartbill = $("#total-bill-cart");
    var price = 0;
    var total = 0;
    $.each(cartData, function (i, item) {
        price = (item['price'] * item['quantity']);
        total += price;
    });

    $cartList.html('');
    $.each(cartData, function (i, item, ) {

        $cartList.append(`
                <tr>
                    <td>
                        <a href=" `+ route +`/`+ item['id']+`"><img src="`+ domain + '/uploads/' + item['thumbnail'] +`" alt="Cart Product Image" title="Cas Meque Metus" class="img-thumbnail"></a>
                    </td>
                  <td>
                        <a href=" `+ route +`/`+ item['id'] +`">`+ item['title'] +`</a>
                 </td>
                  <td>`+ item['id_cate'] +`</td>
                  <td>
                        <form onsubmit="updateQuantityCart(event)" >
                            <div class="input-group btn-block">
                                <input type="number" name="quantity" value="`+item['quantity']+`" class="form-control">
                                <input type="hidden" name="id" value="`+item['id']+`">
                                <span class="input-group-btn">
                                
                                    <button type="submit"  data-toggle="tooltip" data-direction="top" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button>
                                     
                                </span>
                            </div>
                        </form>
                    </td>
                     <td> `+ number_format(item['price']) +` VND
                    </td>
                     <td>`+  number_format(total) +` VND
                    </td>
                    <td>
                     <div class="shopping-cart-delete">
                     <button type="button" onclick="deleteItemCart(`+ item['id'] +`)" href="javascript:void(0);" data-toggle="tooltip" data-direction="top" class="btn btn-danger pull-right" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                       
                    </div>
                    </td>
                   
                   
                </tr>
               `);
    });
    $cartbill.html('');
        $cartbill.append(`
                <tr>
                    <td><strong>Sub-Total:</strong></td>
                    <td` + number_format(total) + `</td>
                    </tr>
                    <tr>
                    <td><strong>Total:</strong></td>
                    <td><span class="primary-color">` + number_format(total)   + `</span></td>
                    </tr>
                    `);
}
function appendCheckout(cartData,cartTotal) {
    $cartList = $("#checkout-list-cart");
    $cartbill = $("#cart-subtotal");
    var price = 0;
    var total = 0;
    $.each(cartData, function (i, item) {
        price = (item['price'] * item['quantity']);
        total += price;
    });
    $cartList.html('');
    $.each(cartData, function (i, item, ) {

        $cartList.append(`
                    <div class="product-list">
                        <div class="product-inner media align-items-center">
                            <div class="product-image mr-4 mr-sm-5 mr-md-4 mr-lg-5">
                                <a href="`+ route +`/`+ item['id'] +`">
                                    <img src="`+ domain + '/uploads/' + item['thumbnail'] +`" alt="Proin Lectus Ipsum" title="Proin Lectus Ipsum">
                                </a>
                            </div>
                            <div class="media-body">
                                <h5>Proin Lectus Ipsum</h5>
                                <p class="product-quantity">Quantity: `+item['quantity']+` VND</p>
                                <p class="product-final-price">`+ number_format(item['price']) +` VND </p>
                            </div>
                        </div>
                    </div>
               `);
    });
    $cartbill.html('');
    $cartbill.append(`
                <tr>
                    <td><strong>Sub-Total:</strong></td>
                    <td>` + number_format(total) + `</td>
                    </tr>
                    <tr>
                    <td><strong>Total:</strong></td>
                    <td><span class="primary-color"> ` + number_format(total) + `</span></td>
                </tr>
                `);

}
function number_format(number, decimals, decPoint, thousandsSep){
    decimals = decimals || 0;
    number = parseFloat(number);

    if(!decPoint || !thousandsSep){
        decPoint = '.';
        thousandsSep = ',';
    }

    var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
    var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
    var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
    var formattedNumber = "";

    while(numbersString.length > 3){
        formattedNumber += thousandsSep + numbersString.slice(-3)
        numbersString = numbersString.slice(0,-3);
    }

    return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}


