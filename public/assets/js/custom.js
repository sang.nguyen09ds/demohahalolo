var domain = "http://e-shop.local/";
var route = domain + '/home/products/detail';

// loadProduct();

$(function(){
    // bind change event to select
    $('.nice-select .list li').click(function () {
        var url = $(this).attr('data-value'); // get selected value
        if (url) { // require a URL
            window.location = url; // redirect
        }
        return false;
    });
});


function clickToCate(id) {
    event.preventDefault();
    jQuery.ajax({
        url: domain + '/v1/products-in-cate/' + id,
        type: "GET",
        success:function(data){
            appendListProductInCate(data);
        },
        error:function (){
            alert("Có lỗi , a không thể thể show sản phẩm");
        }
    });

}

function clickToCatePost(id) {
    event.preventDefault();
    jQuery.ajax({
        url: domain + '/v1/blog-in-cate/' + id,
        type: "GET",
        success:function(data){
            appendListPostInCate(data);
        },
        error:function (){
            alert("Có lỗi ,as  không thể thể show sản phẩm");
        }
    });

}
// function loadProduct() {
//     jQuery.ajax({
//         url: domain + '/v1/products',
//         type: "GET",
//         success:function(data){
//             appendListProduct(data);
//
//         },
//         error:function (){
//             alert("Có lỗi , không thể show sản phẩm");
//         }
//     });
// }
function appendListProduct(data) {
    $listProductInCate = $("#product-in-category");

    $listProductInCate.html('');
    $.each(data.data, function (i, item, ) {
        $listProductInCate.append(`
        
               <div class="product-layout product-grid col-6 col-sm-6 col-md-4 col-lg-4">
                <div class="product-thumb">
                <div class="product-inner">
                <div class="product-image">
                <div class="label-product label-new">New</div>
                <a href="`+ route +`/`+ item['id']  +`">
                <img src="`+ domain +`/uploads/`+ item['thumbnail']  +`" alt="Proin Lectus Ipsum" class="hover-image">
                <img src="`+ domain +`/uploads/`+ item['thumbnail']  +`" alt="Proin Lectus Ipsum" title="Proin Lectus Ipsum">
                </a>
                <div class="action-links">
                <a class="action-btn btn-cart" id="`+ item['id']  +`" onclick="addToCart('http://e-shop-demo.local/home/cart/`+ item['id']+`')" href="javascript:void(0);" title="Add to Cart"><i class="pe-7s-cart"></i></a>
            <a class="action-btn btn-wishlist" href="#" title="Add to Wishlist"><i class="pe-7s-like"></i></a>
            <a class="action-btn btn-compare" href="#" title="Add to Compare"><i class="pe-7s-refresh-2"></i></a>
            <a class="action-btn btn-quickview" data-toggle="modal" data-target="#product_quick_view" href="#" title="Quick View"><i class="pe-7s-search"></i></a>
            </div>
            </div> <!-- end of product-image -->
            
            <div class="product-caption">
                <div class="product-ratings">
                <div class="rating-box">
                <ul class="rating d-flex">
                <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            </ul>
            </div>
            </div>
            <h4 class="product-name"><a href="`+ route +`/`+ item['id']  +`">`+item['title']+`</a></h4>
            <p class="product-price">
                <span class="price-old"> `+ number_format(item['price']) +` VND</span>
                <span class="price-new">`+ number_format(item['price_sale']) +` VND</span>
            </p>
            </div><!-- end of product-caption -->
            </div><!-- end of product-inner -->
            </div><!-- end of product-thumb -->
            </div>
           
        
        `);
    });
    // $pagination = $("#pagination");
    // $pagination.html('');
    // $.each(data, function (i, item, ) {
    //
    //     $pagination.append(`
    //         <div class="row align-items-center">
    //         <div class="col-lg-6 col-md-6">{!! $list->appends(Request::all())->links() !!}</div>
    //         </div>
    //     `);
    //
    // });
}
function appendListProductInCate(data) {

    $listProductInCate = $("#product-in-category");
    $listProductInCate.html('');
    $.each(data, function (i, item, ) {
        $listProductInCate.append(`
               <div class="product-layout product-grid col-6 col-sm-6 col-md-4 col-lg-4">
                <div class="product-thumb">
                <div class="product-inner">
                <div class="product-image">
                <div class="label-product label-new">New</div>
                <a href="`+ route +`/`+ item['id']  +`">
                <img src="`+ domain +`/uploads/`+ item['thumbnail']  +`" alt="Proin Lectus Ipsum" class="hover-image">
                <img src="`+ domain +`/uploads/`+ item['thumbnail']  +`" alt="Proin Lectus Ipsum" title="Proin Lectus Ipsum">
                </a>
                <div class="action-links">
                <a class="action-btn btn-cart" id="`+ item['id']  +`" onclick="addToCart('http://e-shop-demo.local/home/cart/`+ item['id']+`')" href="javascript:void(0);" title="Add to Cart"><i class="pe-7s-cart"></i></a>
            <a class="action-btn btn-wishlist" href="#" title="Add to Wishlist"><i class="pe-7s-like"></i></a>
            <a class="action-btn btn-compare" href="#" title="Add to Compare"><i class="pe-7s-refresh-2"></i></a>
            <a class="action-btn btn-quickview" data-toggle="modal" data-target="#product_quick_view" href="#" title="Quick View"><i class="pe-7s-search"></i></a>
            </div>
            </div> <!-- end of product-image -->
            
            <div class="product-caption">
                <div class="product-ratings">
                <div class="rating-box">
                <ul class="rating d-flex">
                <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            <li><i class="fa fa-star"></i></li>
            </ul>
            </div>
            </div>
            <h4 class="product-name"><a href="`+ route +`/`+ item['id']  +`">`+item['title']+`</a></h4>
            <p class="product-price">
             <span class="price-old"> `+ number_format(item['price']) +` VND</span>
             <span class="price-new">`+ number_format(item['price_sale']) +` VND</span>
            </p>
            </div><!-- end of product-caption -->
            </div><!-- end of product-inner -->
            </div><!-- end of product-thumb -->
            </div>
        `);
    });

}

function appendListPostInCate(data) {

    $listPostInCate = $("#post-list");
    $listPostInCate.html('');
    $.each(data, function (i, item, ) {
        $listPostInCate.append(`
               <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <article class="blog-post post-entry">
                <div class="blog-grid">
                <div class="content-wrapper">
                <div class="post-media">
                <a href="blog-details.html"><img src="`+ domain + '/uploads/post/' + item['thumbnail']+`" alt="Blog Image"></a>
                </div>
                <div class="post-content">
                <ul class="post-category">
                <li><a href="#">Tech</a></li>
                </ul>
                <h3 class="post-title"><a href="blog-details.html">`+ item['title'] +`</a></h3>
                <p>`+ item['description'] +`</p>
                </div>
                <div class="post-footer">
                    <div class="post-meta">
                    <ul>
                    <li>`+ item['date'] +`</li>
                <li>3 Bình luận</li>
                </ul>
                </div>
                <div class="post-more">
                    <a href="blog-details.html">Xem thêm<i class="fa fa-angle-double-right"></i></a>
                </div>
                </div>
                </div>
                </div> <!-- end of blog-grid -->
                </article> <!-- end of blog-post -->
                </div>
             `);
    });

}
